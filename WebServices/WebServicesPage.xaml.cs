﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Net.Http;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace WebServices
{
    public partial class WebServicesPage : ContentPage
    {
        public WebServicesPage()
        {
            InitializeComponent();
        }

        public void Disconnected() {
            ConnectionButton.BackgroundColor = Xamarin.Forms.Color.Red;
            ConnectionButton.Text = "Disconnected";
        }

        public void Connected() {
            ConnectionButton.BackgroundColor = Xamarin.Forms.Color.Lime;
            ConnectionButton.Text = "Connected";
        }

        public class DictionaryJson
        {
            public List<DictionaryData> entries { get; set; }
        }

        public partial class DictionaryData
        {
            [JsonProperty("type")]
            public string type { get; set; }

            [JsonProperty("definition")]
            public string definition { get; set; }

            [JsonProperty("example")]
            public string example { get; set; }
        }

        async void Handle_GetDefinition(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                string word = WordEntry.Text.ToLower();
                HttpClient client = new HttpClient();

                var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + word));
                System.Diagnostics.Debug.WriteLine(uri);

                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    List<DictionaryData> entries = JsonConvert.DeserializeObject<List<DictionaryData>>(content);
                    if (entries.Count > 0)
                    {
                        DefinitionContent.Text = WordEntry.Text + ": " + entries[0].type + "\n\n" + entries[0].definition;
                        if (entries[0].example != null)
                        {
                            DefinitionContent.Text += "\n\nExample: " + entries[0].example;
                        }
                    }
                    else
                    {
                        DefinitionContent.Text = "Sorry! No definitions found for " + WordEntry.Text;
                    }
                } else 
                {
                    DefinitionContent.Text = "There was a problem fetching your definition.\nPlease try again later!";
                }
            }
            else
            {
                DisplayAlert("Alert", "You have lost network connection!", "OK");
                DefinitionContent.Text = "Sorry, you are not connected to a network!";
                ConnectionButton.BackgroundColor = Xamarin.Forms.Color.Red;
            }
        }

        public void CheckNetworkStatus()
        {
            if (CrossConnectivity.Current.IsConnected == true)
            {
                ConnectionButton.Text = "Connected to Internet via " + CrossConnectivity.Current.ConnectionTypes.First();
                ConnectionButton.BackgroundColor = Xamarin.Forms.Color.Lime;
            }
            else
            {
                ConnectionButton.Text = "Not Connected to Internet";
                ConnectionButton.BackgroundColor = Xamarin.Forms.Color.Red;
            }
        }
    }
}
