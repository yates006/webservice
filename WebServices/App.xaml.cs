﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace WebServices
{
    public partial class App : Application
    {
        WebServicesPage wsPage;

        public App()
        {
            InitializeComponent();
            wsPage = new WebServicesPage();
            MainPage = new NavigationPage(wsPage);
        }

        event ConnectivityChangedEventHandler ConnectivityChanged;

        public class ConnectivityChangedEventArgs : EventArgs
        {
            public bool IsConnected { get; set; }
        }

        public delegate void ConnectivityChangedEventHandler(object sender, ConnectivityChangedEventArgs e);

        protected override void OnStart()
        {
            // Handle when your app starts
            /** Connectivity Stuff **/

            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if(CrossConnectivity.Current.IsConnected) {
                    wsPage.Connected();   
                } else {
                    wsPage.Disconnected(); 
                }
            };
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}